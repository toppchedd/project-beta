import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelForm from './Inventory/ModelForm';
import ModelsList from './Inventory/ModelsList';
import SalespeopleList from './Sales/SalespeopleList';
import SalespersonForm from './Sales/SalespersonForm';
import CustomerList from './Sales/Customers';
import CustomerForm from './Sales/CustomerForm';
import SalesForm from './Sales/SalesForm';
import SalesList from './Sales/SalesList';
import SalesHistory from './Sales/SalesHistory';
import ManufacturersList from './ManList';
import ManForm from './ManForm';
import AutoList from './AutoList';
import AutosForm from './AutoForm';
import TechList from './TechList';
import TechForm from './TechForm';
import ServList from './ServList';
import ServForm from './ServForm';
import ServHist from './ServHist';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/models" element={<ModelsList />} />
        <Route path="/create_model" element={<ModelForm />} />
        <Route path="/salespeople" element={<SalespeopleList />} />
        <Route path="/create_salesperson" element={<SalespersonForm />} />
        <Route path="/customer" element={<CustomerList />} />
        <Route path="/create_customer" element={<CustomerForm />} />
        <Route path="/Salesform" element={<SalesForm />} />
        <Route path="/Saleslist" element={<SalesList />} />
        <Route path="/Salesperson_History" element={<SalesHistory />} />
        <Route path="/manufacturers/" element={ <ManufacturersList />} />
        <Route path="/manufacturers/new/" element={ <ManForm />} />
        <Route path="/automobiles/" element={ <AutoList />} />
        <Route path="/automobiles/new/" element={ <AutosForm />} />
        <Route path="/technicians/" element={ < TechList />} />
        <Route path="/technicians/new/" element={ <TechForm />} />
        <Route path="/appointments/" element={ <ServList />} />
        <Route path="/appointments/new/" element={ <ServForm />} />
        <Route path="/appointment/history" element={ <ServHist />} />
      </Routes>
    </BrowserRouter>
  );
}
export default App;
