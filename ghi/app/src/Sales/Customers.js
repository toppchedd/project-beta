import React, { useState, useEffect } from 'react'; 

function CustomerList({}) {
    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const Url = "http://localhost:8090/api/customers/";

        const Response = await fetch(Url);
        
        if (Response.ok) {
            const data = await Response.json();
            setCustomers(data.customers); 
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Customers</h1> 
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((customer, index)=> (
                        <tr key={index}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.phone_number}</td>
                            <td>{customer.address}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default CustomerList;