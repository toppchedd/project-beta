import React, { useState, useEffect } from 'react';

function SalesForm() {
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    });

    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchUnsoldAutomobiles = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/automobiles/');
                if (!response.ok) {
                    throw new Error(`Errors: ${response.status}`);
                }
                const automobilesData = await response.json();
                console.log(automobilesData); 
                const unsoldAutomobiles = automobilesData.data.filter(auto => !auto.sold);
                setAutomobiles(unsoldAutomobiles);
            } catch (error) {
                console.error('Errors: ' + error.message);
            }
            
        };

        const fetchSalespeople = async () => {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            }
        };

        const fetchCustomers = async () => {
            const response = await fetch('http://localhost:8090/api/customers/');
            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customers);
            }
        };

        fetchUnsoldAutomobiles();
        fetchSalespeople();
        fetchCustomers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
    
        const url = 'http://localhost:8090/api/sales/'; 
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: { 'Content-Type': 'application/json' },
        };
    
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({ 
                automobile: '', 
                salesperson: '', 
                customer: '', 
                price: '' });
        }
    };

    const handleFormChange = (e) => {
        setFormData({ 
            ...formData, 
            [e.target.name]: e.target.value 
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        
                    <div className="form-group mb-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-control">
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles.map(auto => (
                                    <option key={auto.id} value={auto.id}>
                                        {auto.vin}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="form-group mb-3">
                            <label htmlFor="salesperson">Salesperson</label>
                            <select onChange={handleFormChange} value={formData.salesperson} required name="salesperson" id="salesperson" className="form-control">
                                <option value="">Choose a Salesperson...</option>
                                {salespeople.map(person => (
                                    <option key={person.employee_id} value={person.employee_id}>
                                        {person.first_name} {person.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="form-group mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-control">
                                <option value="">Choose a Customer...</option>
                                {customers.map(cust => (
                                    <option key={cust.id} value={cust.id}>
                                        {cust.first_name} {cust.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="form-group mb-3">
                            <label htmlFor="price">Price</label>
                            <input onChange={handleFormChange} value={formData.price} required type="text" className="form-control" id="price" name="price"  />
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SalesForm;
