import React, { useState, useEffect } from 'react';

function SalespersonSalesHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [customers, setCustomers] = useState([]);

    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople); 
        }
    };

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers); 
        }
    };

    const handleSalespersonChange = (event) => {
        setSelectedSalesperson(event.target.value); 
    };

    useEffect(() => {
        fetchSalespeople();
        fetchCustomers();
    }, []);

    return (
        <div>
            <h1>Salesperson History</h1>

            <div>
                <select value={selectedSalesperson} onChange={handleSalespersonChange}>
                    <option value="">Choose Salespeople...</option>
                    {salespeople.map(salesperson => (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.first_name + ' ' + salesperson.last_name}
                        </option>
                    ))}
                </select>
            </div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.filter(salesperson => selectedSalesperson === '' || salesperson.employee_id === selectedSalesperson)
                                .map((salesperson, index) => (
                        <tr key={salesperson.employee_id}>
                            <td>{salesperson.first_name + " " + salesperson.last_name}</td>
                            <td>{index < customers.length ? `${customers[index].first_name} ${customers[index].last_name}` : 'None'}</td>
                            <td>{"4Y1SL65848Z411439."}</td>
                            <td>{"20,000"}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonSalesHistory;