import { NavLink } from 'react-router-dom';

function Nav() {
  return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0" style={{flexWrap:"wrap", alignItems:"baseline"}}>
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/models">Models</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/create_model">Create a Model</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/salespeople">Salespeople</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/create_salesperson">Add a Salesperson</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/customer">Customers</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/create_customer">Add a Customers</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/Saleslist">Sales</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/Salesform">Add a Sale</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/Salesperson_History">Salesperson History</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/manufacturers/">List Manufacturers</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/manufacturers/new/">Create A Manufacturer</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/automobiles/">List Automobiles</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/automobiles/new/">Create An Automobile</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/technicians/">List Technicians</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/technicians/new/">Create A Technician</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/appointments/">List Appointments</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/appointments/new/">Create An Appointment</NavLink>
        </li>
        <li className="nav-link">
              <NavLink className="navbar-brand" to="/appointment/history/">Service History</NavLink>
        </li>
        </ul>
        </div>
      </div>
      </nav>
  )
}

export default Nav;
