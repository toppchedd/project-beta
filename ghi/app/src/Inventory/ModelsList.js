import React, { useState, useEffect } from 'react'; 

function ModelsList({}) {
    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const vehicleUrl = "http://localhost:8100/api/models/";

        const Response = await fetch(vehicleUrl);
        
        if (Response.ok) {
            const data = await Response.json();
            setModels(data.models); 
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(vehicle => (
                        <tr key={vehicle.id}>
                            <td>{vehicle.name}</td>
                            <td>{vehicle.manufacturer.name}</td> 
                            <td><img src={vehicle.picture_url} alt="" width="100px" height="100px" /></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ModelsList;