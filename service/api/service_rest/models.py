from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=50)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField(default=None, blank=True, null=True)
    service_reason = models.CharField(max_length=500)
    status = models.CharField(max_length=20, default="TBD")
    vin = models.CharField(max_length=20)
    customer = models.CharField(max_length=300)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
