from django.urls import path, include
from .views import api_technician, api_appointment, api_appointment_detail, api_technician_detail


urlpatterns = [
    path("technicians/", api_technician, name="technicians"),
    path("appointments/", api_appointment, name="appointments"),
    path('api/appointments/<int:id>/', api_appointment_detail, name='api_appointment_detail'),
    path('api/technicians/<int:id>/', api_technician_detail, name='api_technician_detail'),
]
