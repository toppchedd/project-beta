from django.contrib import admin

# Register your models here.
from .models import Salesperson, Customer, AutomobileVO, Sale

admin.site.register(Salesperson)
admin.site.register(Customer)
admin.site.register(AutomobileVO)
admin.site.register(Sale)