from django.urls import path
from .views import api_salesperson, api_customer, api_sales, api_sales_details

urlpatterns = [
    path('salespeople/', api_salesperson, name='api_list_salesperson'),
    path('salespeople/', api_salesperson, name='create_salesperson'),
    path('salespeople/<int:pk>/',api_salesperson, name='delete_salesperson'),

    path('customers/', api_customer, name='api_list_customers'),
    path('customers/', api_customer, name='api_create_customer'),
    path('customers/<int:pk>/', api_customer, name='api_delete_customer'),

    path('sales/', api_sales, name='api_list_sales'),
    path('sales/', api_sales, name='api_create_sale'),
    path('sales/<int:id>/', api_sales_details, name='api_delete_sale'),
]