from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Salesperson, Customer, AutomobileVO, Sale


class SalesPersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id", 
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
    }


@require_http_methods(["GET", "POST", "DELETE"])
def api_salesperson(request, pk=None):
    if request.method == "GET":
        if pk:
            try:
                salesperson = Salesperson.objects.get(id=pk)
                return JsonResponse(
                    salesperson, 
                    encoder=SalesPersonListEncoder, 
                    safe=False
                    )
            except Salesperson.DoesNotExist:
                response = JsonResponse({"message": "Does not exist"})
                response.status_code = 404
                return response
        else:
            salespeople = Salesperson.objects.all()
            return JsonResponse({"salespeople": salespeople}, encoder=SalesPersonListEncoder)

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(**data)
            return JsonResponse(salesperson, encoder=SalesPersonListEncoder, safe=False)
        except:
            response = JsonResponse({"message": "Cannot create salesperson"})
            response.status_code = 400
            return response

    else: 
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(salesperson, encoder=SalesPersonListEncoder, safe=False)
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST", "DELETE"])
def api_customer(request, pk=None):
    if request.method == "GET":
        if pk:
            try:
                customer = Customer.objects.get(pk=pk)
                return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
            except Customer.DoesNotExist:
                response = JsonResponse({"message": "Customer does not exist"})
                response.status_code = 404
                return response
        else:
            customers = Customer.objects.all()
            return JsonResponse({"customers": customers}, encoder=CustomerListEncoder)

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            customer = Customer.objects.create(**data)
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except Exception as e:
            response = JsonResponse({"message": "Cannot create customer"})
            response.status_code = 400
            return response

    elif request.method == "DELETE":
        if pk is not None:
            try:
                customer = Customer.objects.get(pk=pk)
                customer.delete()
                return JsonResponse({}, status=200)
            except Customer.DoesNotExist:
                response = JsonResponse({"message": "Customer does not exist"})
                response.status_code = 404
                return response

        return JsonResponse({"message": "Does not exist"}, status=400)


@require_http_methods(["GET", "POST"])
def api_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sale = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sale = Sale.objects.all()
        return JsonResponse({"sales": sale}, encoder=SaleEncoder)
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile VIN invalid"}, status=400)
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson id invalid"}, status=400)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer id invalid"}, status=400)
        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)

@require_http_methods(["DELETE"])
def api_sales_details(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse({"deleted": True}, content_type="application/json")
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404, content_type="application/json")
